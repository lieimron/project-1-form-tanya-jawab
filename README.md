# Forum Tanya Jawab (Tugas Kel 19)

## About

Aplikasi ini dibuat oleh Kelompok 19 PKS Digital School sebagai penugasan dalam Project 1

## Pengembang

-   Rizki Aditya Suherdi (@RizkiAdityas)
-   Ali Imron (@lieimron)
-   Edy Santoso (@achedy)

## Pembagian Kerja

### BASIC DEV

Dikerjakan oleh @lieimron

-   1.1. Membuat ERD Forum berdasarkan tugas yg diberikan
-   1.2. Implementasi ERD di Migration
-   1.3. Membuat template desain aplikasi (Menggunakan adminLTE 3)
-   1.4. Upload ke Gitlab

### MIDDLE DEV

Dikerjakan oleh @RizkiAdityas + @achedy

-   2.1. Modul Auth + User -> pakai Sweet alert ( @achedy )
-   2.2. Modul Kategori -> pakai Sweet alert dan datatables ( @achedy )
-   2.3. Modul Pertanyaan -> pakai Lib Tiny MCE , file manager (@RizkiAdityas)
-   2.4. Modul Jawaban -> pakai Lib Tiny MCE, file manager (@RizkiAdityas)

## ERD

ERD dbibuat oleh @lieimron

![alt text](erd.jpg)

## Laravel License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
