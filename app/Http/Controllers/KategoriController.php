<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;



class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Kategori::orderBy('id', 'DESC')->get();
        return view('kategori.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nama' => 'required'
        ]);

        Kategori::create([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'user_id' => Auth::id()
        ]);

        Alert::success('Tambah', 'Hore .. tambah kategori berhasil');
        return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        $show = $kategori;
        return view('kategori.show', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {

        $show = $kategori;

        if ($show->user_id != Auth::id()) {
            return redirect('/kategori');
        }

        return view('kategori.edit', compact('show'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        //
        $this->validate($request, [
            'nama' => 'required',

        ]);



        $kategori = Kategori::find($request->id);


        if ($kategori->user_id != Auth::id()) {
            return redirect('/kategori');
        }


        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->update();
        Alert::success('Update', 'Hore .. update kategori berhasil');
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        if ($kategori->user_id != Auth::id()) {
            return redirect('/kategori');
        } else {
            $kategori = Kategori::find($kategori->id);
            $kategori->delete();
            Alert::success('Hapus', 'Hore .. hapus kategori berhasil');
            return redirect('/kategori');
        }
    }
}
