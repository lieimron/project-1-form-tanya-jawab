<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;


class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'umur' => ['required'],
            'alamat' => ['required'],
            'bio' => ['required'],
        ]);
    }


    public function show()
    {
        $auth = Auth::user();
        $user = User::find($auth->id);
        return view('profile.profile', compact('user'));
    }

    public function edit()
    {
        $auth = Auth::user();
        $user = User::find($auth->id);
        return view('profile.edit', compact('user'));
    }

    public function Update(Request $request)
    {
        $auth = Auth::user();
        $user = User::find($auth->id);
        $user->name = $request->name;
        $user->update();

        $profile = Profile::find($user->profile->user_id);
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->bio = $request->bio;
        $profile->update();
        Alert::success('Update Profile', 'Hore .. Update profile berhasil');
        return redirect('/profile');
    }
}
