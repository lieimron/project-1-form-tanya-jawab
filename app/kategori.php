<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['nama', 'deskripsi', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
