@extends('layout.master')
@section('Judul')
Edit Kategori
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/kategori">Kategori</a></li>
<li class="breadcrumb-item active">Edit Kategori</li>
@endsection

@section('content')
<form action="/kategori/{{$show->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$show->nama}}" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="deskripsi">Deskripsi</label>
        <input type="text" class="form-control" name="deskripsi" id="umur" value="{{$show->deskripsi}}" placeholder="Masukkan Deskripsi">
        @error('usia')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>   
    <input type="hidden" name="id" value="{{$show->id}}"  />
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection