@extends('layout.master')
@section('Judul')
  Kategori
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active">Master Kategori</li>
@endsection

@section('content')
<a class="btn btn-success btn-sm mb-3" href="/kategori/create">Tambah Kategori</a>
<table id="example1" class="table table-bordered table-striped">
  <thead>
  <tr>
    <th>ID</th>
    <th>Nama</th>
    <th>Deskripsi</th>
    <th>User</th>    
    <th>Aksi</th>
  </tr>
  </thead>
  <tbody>
  @forelse ($data as $key=>$value)
  <tr>
    <td>{{$value->id}}</td>
    <td>{{$value->nama}}</td>
    <td>{{$value->deskripsi}}</td>
    <td>{{$value->user->name}}</td>

    <td>
      <form action="/kategori/{{$value->id}}" method="POST">
      <a class="btn btn-success btn-xs" href="/pertanyaan/{{$value->id}}">Pertanyaan</a>
      @if(Auth::id() == $value->user->id)       
          @csrf
          @method('delete')
        <a class="btn btn-warning btn-xs" href="/kategori/{{$value->id}}/edit">Edit</a>
        <input type="submit" class="btn btn-danger btn-xs" value="Delete" onclick="return confirm('Are You sure?')">
      @endif
    </form>

    </td>
  </tr>
  @empty
  <tr colspan="4">
      <td>No data</td>
  </tr>  
  @endforelse   
  </tbody>        
  </table> 
@endsection

@push('script')
<script src="{{asset('/admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
