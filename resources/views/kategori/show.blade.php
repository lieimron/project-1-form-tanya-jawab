@extends('layout.master')
@section('Judul')

Cast Show / {{$show->nama}}
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/cast">Cast</a></li>
<li class="breadcrumb-item active">Cast Show</li>
@endsection

@section('content')
<table class="table table-bordered">
    <tbody>
        <tr>
            <td>ID</td>
            <td>{{$show->id}}</td>
        </tr>
        <tr>
            <td>Kategori</td>
            <td>{{$show->nama}}</td>
        </tr>
        <tr>
            <td>Deskripsi</td>
            <td>{{$show->deskripsi}}</td>
        </tr>
    </tbody>    
</table>
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
