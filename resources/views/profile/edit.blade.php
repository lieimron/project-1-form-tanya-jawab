@extends('layout.master')
@section ('Judul')
profile
@endsection

@section ('content')
<form method="POST" action="/profile">
    @csrf

    @method('put')

    <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

        <div class="col-md-6">
            {{$user->email}}
        </div>
    </div>

    
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" required autocomplete="name" autofocus>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="umur" class="col-md-4 col-form-label text-md-right">Umur</label>

        <div class="col-md-6">
            <input id="umur" type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{$user->profile->umur}}" required autofocus>

            @error('umur')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

        <div class="col-md-6">
            <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ $user->profile->alamat }}" required autocomplete="name" autofocus>

            @error('alamat')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="bio" class="col-md-4 col-form-label text-md-right">Bio</label>

        <div class="col-md-6">
            <textarea id="bio" class="form-control" name="bio">{{ $user->profile->bio }}</textarea>
            @error('bio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>



    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Update
            </button>
        </div>
    </div>
</form>

@endsection