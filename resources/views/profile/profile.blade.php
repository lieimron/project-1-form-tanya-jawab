@extends('layout.master')
@section ('Judul')
Profile
@endsection

@section ('content')
<table class="table table-bordered">   
    <tr>
        <th>Email</th>
        <td>{{$user->email}}</td>
    </tr>
    <tr>
        <th>Nama</th>
        <td>{{$user->name}}</td>
    </tr>
    <tr>
        <th>Umur</th>
        <td>{{$user->profile->umur}}</td>
    </tr>
    <tr>
        <th>Alamat</th>
        <td>{{$user->profile->alamat}}</td>
    </tr>
    <tr>
        <th>Bio</th>
        <td>{{$user->profile->bio}}</td>
    </tr>
    <tr>
        <th></th>
        <td><a href="/profile/edit" class="btn btn-primary">Edit Profile</a></td>
    </tr>
</table>
@endsection