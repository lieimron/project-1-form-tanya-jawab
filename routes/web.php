<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/open', function () {
    return view('open');
});

Route::get('/master', function () {
    return view('layout.master');
});

//CRUD kategori
//Create database
route::get('/kategori/create', 'KategoriController@create'); // route menuju form create
route::post('/kategori', 'KategoriController@store'); // route untuk menyimpan data ke database


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//// @Cakedy
Route::get('/', function () {
    return redirect()->to('/login');
});

// profile
Route::get('/profile/', 'ProfileController@Show');
Route::get('/profile/edit', 'ProfileController@Edit');
Route::put('/profile', 'ProfileController@Update');

// kategori
Route::resource('kategori', KategoriController::class);

//// @endCakedy
